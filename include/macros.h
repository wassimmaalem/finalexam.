/*
 * macros.h
 *
 *  Created on: Apr 18, 2020
 *      Author: zamek
 */

#ifndef MACROS_H_
#define MACROS_H_

#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>

#define MALLOC(addr, size)  addr = malloc(size); if(addr == NULL) {perror("malloc"); exit(1);}
#define FREE(addr)  free(addr)

#endif /* MACROS_H_ */
