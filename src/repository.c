/*
 * repository.c
 *
 *  Created on: Apr 23, 2020
 *      Author: zamek
 */

#include <stdlib.h>
#include <syslog.h>
#include <pthread.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <repository.h>
#include <macros.h>

#define DEBUG
#define BUFFER_SIZE 256
#define DELIMITER ";"

#define ERR_REPO_NOT_INITIALIZED "repository not initialized"
#define ERR_REPO_ALREADY_INITIALIZED "repository already initialized"
#define ERR_MUTEX_LOCK_FAILED "Mutex lock failed"
#define ERR_MUTEX_UN_LOCK_FAILED "Mutex unlock failed"
#define ERR_TYPE_INVALID "type invalid"
#define ERR_NAME_IS_EMPTY_OR_NULL "name is empty or null"
#define ERR_PIECES_IS_ZERO_OR_NEGATIVE "pieces is zero or negative"
#define ERR_NAME_IS_EMPTY_OR_NULL "name is empty or null"
#define ERR_TYPE_IS_WRONG "type is wrong"
#define ERR_LINE_IS_EMPTY "Line is empty"

/**
 * @brief Repository structure
 *
 * Represents the repository of units
 */
static struct {
	int initialized;	/**< Is the repository initialized */
	pthread_mutex_t mutex;	/**< Mutex for synchronization */
	pthread_cond_t cond;	/**< Condition for dequeue */
	TAILQ_HEAD(p_list, unit_t)
	head;		/**< List of units */
} repository = {
		.initialized = 0,
		.mutex = PTHREAD_MUTEX_INITIALIZER,
		.cond = PTHREAD_COND_INITIALIZER
};

/**
 * @brief chomp a string
 *
 * Remove \r/\n from end of a string
 * @param buffer String to be cleaned from newlines
 * @return The buffer without newlines
 */
static char *str_chomp(char *buffer) {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter str_chomp, buffer:%s", buffer);
#endif

	if (!buffer || !*buffer)
		return (buffer);

	char *begin = buffer;

	while (*buffer++)
		;

	while (--buffer >= begin) {
		if (*buffer >= ' ')
			return (begin);

		if (*buffer == '\r' || *buffer == '\n')
			*buffer = '\0';
	}
	return begin;
}

/**
 * @brief Create and initialize a unit_t
 *
 * Create, initialize and validate a unit_t
 * @param type Unit type
 * @param pieces How much pieces the unit has
 * @param unit_price Unit price
 * @param name Unit name
 * @return Pointer to the created unit
 */
static struct unit_t* create_unit(enum type_t type, int pieces, int unit_price,
		const char *name) {
#ifdef DEBUG
	syslog(LOG_DEBUG,
			"Enter create_unit, type:%d, pieces:%d, unit_price:%d, name: %s",
			type, pieces, unit_price, name);
#endif
	if (!(type >= CAR && type <= UNKNOWN && pieces >= 0 && unit_price >= 0
			&& name && *name)) {
		syslog(LOG_WARNING, "create_unit input error");
		return NULL;
	}

	struct unit_t *result;
	MALLOC(result, sizeof(struct unit_t));
	result->type = type;
	result->pieces = pieces;
	result->unit_price = unit_price;
	result->name = strdup(name);

	return result;
}

/**
 * @brief Process a repository file line
 *
 * Splits the line and creates a unit, which is inserted into repository
 * @param line String representing the line
 */
static void process_line(char *line) {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter process_line, line:%s", line);
#endif
	if (!(line && *line)) {
		syslog(LOG_WARNING, ERR_LINE_IS_EMPTY);
		return;
	}

	//split line and fill the unit
	char * token = strtok(line, DELIMITER);
	int type	 = atoi(token);

	token = strtok(NULL, DELIMITER);
	int pieces = atoi(token);

	token = strtok(NULL, DELIMITER);
	int unit_price = atoi(token);

	token = strtok(NULL, DELIMITER);
	char *name = strdup(token);

	struct unit_t *u = create_unit(type, pieces, unit_price, name);
	if(u){
		//add to repository list
		TAILQ_INSERT_TAIL(&repository.head, u, next);
	}
}

/**
 * @brief Load repository file
 *
 * Checks filename, and the loads the repository file.
 * @param file Name of the repository file
 * @return EXIT_SUCCESS or EXIT_FAILURE
 */
static int load_from_file(const char *file) {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter load_from_file, file:%s", file);
#endif
	if (!(file && *file)) {
		syslog(LOG_ERR, "file is null or empty");
		return EXIT_SUCCESS;
	}

	FILE *f = fopen(file, "r");
	if (!f) {
		syslog(LOG_WARNING, "cannot open file, result:%s", strerror(errno));
		return errno == ENOENT ? EXIT_SUCCESS : EXIT_FAILURE;
	}

	char *line;
	MALLOC(line, BUFFER_SIZE);
	while ((line = fgets(line, BUFFER_SIZE, f))){
		str_chomp(line);
		process_line(line);
	}

	FREE(line);
	fclose(f);
	return EXIT_SUCCESS;
}

/**
 * @brief Saves repository to disk
 *
 * Checks filename, and the saves the repository to specified file.
 * @param file Name of the repository file
 * @return EXIT_SUCCESS or EXIT_FAILURE
 */
static int save_to_file(const char *file) {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter save_to_file, file:%s", file);
#endif

	if (!(file && *file)) {
		syslog(LOG_ERR, "file is null or empty");
		return EXIT_FAILURE;
	}

	FILE *f = fopen(file, "w");
	if (!f) {
		syslog(LOG_WARNING, "cannot open file, result:%s", strerror(errno));
		return EXIT_FAILURE;
	}

	struct unit_t * u;
	TAILQ_FOREACH(u, &repository.head, next) {
		fprintf(f, "%d%s%d%s%d%s%s\n", u->type, DELIMITER, u->pieces, DELIMITER, u->unit_price, DELIMITER, u->name);
	}

	fclose(f);
	return EXIT_SUCCESS;
}

/**
 * @brief Open a repository
 *
 * Open a repository and initialise its members. All units are loaded from file.
 * @param file Name of the repository file
 * @return EXIT_SUCCESS or EXIT_FAILURE
 */
int rep_open(const char *file) {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter rep_open, file:%s", file);
#endif

	if (repository.initialized) {
		syslog(LOG_ERR, "repository already initialized");
		return EXIT_FAILURE;
	}

	TAILQ_INIT(&repository.head);

	if (pthread_mutex_init(&repository.mutex, NULL)) {
		syslog(LOG_ERR, "Cannot initialize mutex");
		return EXIT_FAILURE;
	}

	if (pthread_cond_init(&repository.cond, NULL)) {
		syslog(LOG_ERR, "Cannot initialize mutex");
		return EXIT_FAILURE;
	}

	if (file && *file && (load_from_file(file) != EXIT_SUCCESS)) {
		syslog(LOG_WARNING, "File load error %s", file);
		return EXIT_FAILURE;
	}

	repository.initialized = 1;
	return EXIT_SUCCESS;
}

/**
 * @brief Close a repository
 *
 * Close the repository, saving all its units to the file, and releasing memory/locks.
 * @param file Name of the repository file
 * @return EXIT_SUCCESS or EXIT_FAILURE
 */

int rep_close(const char *file) {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter rep_close file:%s", file);
#endif
	if (!repository.initialized) {
		syslog(LOG_ERR, ERR_REPO_NOT_INITIALIZED);
		return EXIT_FAILURE;
	}

	if (file && *file && (save_to_file(file) != EXIT_SUCCESS)) {
		syslog(LOG_ERR, "Cannot save file:%s", file);
		return EXIT_FAILURE;
	}

	while (!TAILQ_EMPTY(&repository.head)) {
		struct unit_t *u = TAILQ_FIRST(&repository.head);
		TAILQ_REMOVE(&repository.head, u, next);
	}

	if (pthread_mutex_destroy(&repository.mutex)) {
		syslog(LOG_ERR, "Cannot destroy mutex");
		return EXIT_FAILURE;
	}

	if (pthread_cond_destroy(&repository.cond)) {
		syslog(LOG_ERR, "Cannot destroy mutex");
		return EXIT_FAILURE;
	}

	repository.initialized = 0;
	return EXIT_SUCCESS;
}

/**
 * @brief Enqueue a unit to the repository
 *
 * Creates a unit, and adds it to the repository
 * @param type Unit type
 * @param name Unit name
 * @param pieces How much pieces the unit has
 * @param unit_price Unit price
 * @return EXIT_SUCCESS or EXIT_FAILURE
 */
int rep_enqueue(enum type_t type, const char *name, int pieces, int unit_price) {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter rep_enqueue, type:%d name:%s, pieces:%d", type, name,
			pieces);
#endif

	if(!repository.initialized){
		return EXIT_FAILURE;
	}

	pthread_mutex_lock(&repository.mutex);
	int rv = EXIT_FAILURE;
	struct unit_t * u = create_unit(type, pieces, unit_price, name);
	if(u){
		TAILQ_INSERT_TAIL(&repository.head, u, next);
		rv = EXIT_SUCCESS;
	}
	pthread_cond_signal(&repository.cond);
	pthread_mutex_unlock(&repository.mutex);
	return rv;
}

/**
 * @brief Find a unit by type and name
 *
 * Searches for the unit in repository, using its name
 * @param type Unit type
 * @param name Unit name
 * @return NULL if unit is not found, unit pointer otherwise
 */
struct unit_t *rep_find_unit(enum type_t type, const char *name) {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter rep_find_unit type:%d, name:%s", type, name);
#endif
	if (!repository.initialized) {
		syslog(LOG_ERR, ERR_REPO_NOT_INITIALIZED);
		return NULL;
	}

	if(name == NULL){
		return NULL;
	}

	struct unit_t * u;
	TAILQ_FOREACH(u, &repository.head, next) {
		if((u->type == type) && (strcmp(name, u->name) == 0)){
			break;
		}
	}

	return u;
}

/**
 * @brief Dequeue a unit from the repository
 *
 * Removes a number of pieces from the specified unit.
 * @param type Unit type
 * @param name Unit name
 * @param pieces How much pieces the unit has
 * @return NULL if unit is not found, unit pointer otherwise
 */
struct unit_t *rep_dequeue(enum type_t type, const char *name, int pieces) {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter rep_output, type:%d, name:%s, pieces:%d", type,
			name, pieces);
#endif

	if(!repository.initialized){
		return NULL;
	}

	struct unit_t *u = rep_find_unit(type, name);
	if(u != NULL){
		pthread_mutex_lock(&repository.mutex);

		while(u->pieces < pieces){
			pthread_cond_wait(&repository.cond, &repository.mutex);
		}
		u->pieces -= pieces;
		pthread_mutex_unlock(&repository.mutex);
	}

	return u;
}

/**
 * @brief Count units
 *
 * Count units from the specified type
 * @param type Unit type
 * @return sum of all units
 */
int rep_count_type(enum type_t type) {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter rep_count_type, type:%d", type);
#endif
	int sum = 0;
	struct unit_t * u;

	pthread_mutex_lock(&repository.mutex);

	TAILQ_FOREACH(u, &repository.head, next) {
		if(u->type == type){
			sum += u->pieces;
		}
	}
	pthread_mutex_unlock(&repository.mutex);
	return sum;
}

/**
 * @brief Sum price of all units
 *
 * Sum the price of all units in repository
 * @return price of all units
 */
int rep_sum_price() {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter rep_sum_price");
#endif
	int sum = 0;

	struct unit_t * u;

	pthread_mutex_lock(&repository.mutex);

	TAILQ_FOREACH(u, &repository.head, next) {
		sum += u->unit_price;
	}
	pthread_mutex_unlock(&repository.mutex);

	return sum;
}

/**
 * @brief Free a unit pointer
 *
 * Release memory for unit name and pointer
 * @param u The unit pointer
 * @return EXIT_SUCCESS if pointer is not null
 */
int rep_free_unit(struct unit_t *u) {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter rep_sum_unit, u is null:%s", u ? "false" : "true");
#endif
	if (!u) {
		syslog(LOG_WARNING, "rep_free_unit u is null");
		return EXIT_FAILURE;
	}

	if(u == NULL){
		return EXIT_FAILURE;
	}

	if(u->name){
		free(u->name);
	}
	free(u);

	return EXIT_SUCCESS;
}

/**
 * @brief Check if repository is empty
 *
 * Checks if the repository list of units is empty
 * @return true or false
 */
int rep_is_empty() {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter rep_is_empty");
#endif

	if(!repository.initialized){
		return 1;
	}

	pthread_mutex_lock(&repository.mutex);
	int rv = TAILQ_EMPTY(&repository.head);
	pthread_mutex_unlock(&repository.mutex);
	return rv;
}
