#Makefile for repository

APP=repository

DEBUG=yes
SRC=src
BIN=bin
INCLUDE=include
LIBS=-l cunit -l pthread

CFLAGS=-Wall -pedantic -std=c99 -D_DEFAULT_SOURCE

ifeq ($(DEBUG),yes)
CFLAGS+=-g
endif

all: clean $(BIN)/$(APP) docs

docs: Doxyfile
	mkdir -p docs
	doxygen

clean:
		$(RM) -r $(BIN) docs

$(BIN)/$(APP): src/repository.c include/repository.h include/macros.h src/test.c
	mkdir -p bin
	gcc $(CFLAGS) -Iinclude src/repository.c src/test.c -o $(BIN)/$(APP) $(LIBS)

test: clean $(BIN)/$(APP)
		$(BIN)/$(APP)

.PHONY: clean all
